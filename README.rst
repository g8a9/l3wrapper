l3wrapper
=========

.. image:: https://img.shields.io/pypi/v/l3wrapper.svg
    :target: https://pypi.python.org/pypi/l3wrapper
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal.png
   :target: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal
   :alt: Latest Travis CI build status

A simple Python 3 wrapper around L3 binaries.

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`l3wrapper` was written by `Giuseppe Attanasio <giuseppe.attanasio@polito.it>`_.
